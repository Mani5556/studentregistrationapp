package com.student_registration.models;

import java.io.Serializable;
import java.util.Calendar;
import java.util.TreeMap;

public class StudentModel implements Serializable {
	private static final long serialVersionUID = 6529685098267757690L;
	public static final String PASSWORD = "Inno@1234";
	public static final int OC_FEES = 50000;
	public static final int BC_FEES = 45000;
	public static final int SC_FEES = 40000;
	public static final int ST_FEES = 35000;
	public static final int SYSTEM_YEAR = Calendar.getInstance().get(Calendar.YEAR);
	public static final int SYSTEM_MONTH = Calendar.getInstance().get(Calendar.MONTH);

	private String studentId;
	private String firstName;
	private String lastname;
	private String dateOfBirth;
	private String mobileNo;
	private String email;
	private String gender;
	private String address;
	private String Qualification;
	private String caste;
	private double fees;
	private String course;
	private double due;
	private double totalFee;
	private String userName;
	private String passWord;
	private String adharNumber;
	private int JoiningYear;
	private String favoritePlace;
	
	
	
	/**
	 * this method returns the favorite place 
	 * @return
	 */
	public String getFavoritePlace() {
		return favoritePlace;
	}

	public void setFavoritePlace(String favoritePlace) {
		this.favoritePlace = favoritePlace;
	}

	private TreeMap<String,StudentMarks> marks ;
	private int Backlogs;
	/**
	 * @return the backlogs
	 */
	public int getBacklogs() {
		return Backlogs;
	}

	/**
	 * @param backlogs the backlogs to set
	 */
	public void setBacklogs(int backlogs) {
		Backlogs = backlogs;
	}

	/**
	 * @return the marks
	 */
	public TreeMap<String, StudentMarks> getMarks() {
		return marks;
	}

	/**
	 * @param marks the marks to set
	 */
	public void setMarks(TreeMap<String, StudentMarks> marks) {
		this.marks = marks;
	}

	/**
	 * @return the joiningYear
	 */
	public int getJoiningYear() {
		return JoiningYear;
	}

	/**
	 * @param joiningYear the joiningYear to set
	 */
	public void setJoiningYear(int joiningYear) {
		JoiningYear = joiningYear;
	}

	private String SecurityQue = "";

	/**
	 * @return the studentId
	 */
	public String getStudentId() {
		return studentId;
	}

	/**
	 * @param studentId the studentId to set
	 */
	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastname
	 */
	public String getLastname() {
		return lastname;
	}

	/**
	 * @param lastname the lastname to set
	 */
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	/**
	 * @return the dateOfBirth
	 */
	public String getDateOfBirth() {
		return dateOfBirth;
	}

	/**
	 * @param dateOfBirth the dateOfBirth to set
	 */
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	/**
	 * @return the mobileNo
	 */
	public String getMobileNo() {
		return mobileNo;
	}

	/**
	 * @param mobileNo the mobileNo to set
	 */
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * @param gender the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return the qualification
	 */
	public String getQualification() {
		return Qualification;
	}

	/**
	 * @param qualification the qualification to set
	 */
	public void setQualification(String qualification) {
		Qualification = qualification;
	}

	/**
	 * @return the caste
	 */
	public String getCaste() {
		return caste;
	}

	/**
	 * @param caste the caste to set
	 */
	public void setCaste(String caste) {
		this.caste = caste;
	}

	/**
	 * @return the fees
	 */
	public double getFees() {
		return fees;
	}

	/**
	 * @param fees the fees to set
	 */
	public void setFees(double fees) {
		this.fees = fees;
	}

	/**
	 * @return the course
	 */
	public String getCourse() {
		return course;
	}

	/**
	 * @param course the course to set
	 */
	public void setCourse(String course) {
		this.course = course;
	}

	/**
	 * @return the due
	 */
	public double getDue() {
		return due;
	}

	/**
	 * @param due the due to set
	 */
	public void setDue(double due) {
		this.due = due;
	}

	/**
	 * @return the totalFee
	 */
	public double getTotalFee() {
		return totalFee;
	}

	/**
	 * @param totalFee the totalFee to set
	 */
	public void setTotalFee(double totalFee) {
		this.totalFee = totalFee;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the passWord
	 */
	public String getPassWord() {
		return passWord;
	}

	/**
	 * @param passWord the passWord to set
	 */
	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}

	/**
	 * @return the adharNumber
	 */
	public String getAdharNumber() {
		return adharNumber;
	}

	/**
	 * @param adharNumber the adharNumber to set
	 */
	public void setAdharNumber(String adharNumber) {
		this.adharNumber = adharNumber;
	}

	/**
	 * @return the securityQue
	 */
	public String getSecurityQue() {
		return SecurityQue;
	}

	/**
	 * @param securityQue the securityQue to set
	 */
	public void setSecurityQue(String securityQue) {
		SecurityQue = securityQue;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the password
	 */
	public static String getPassword() {
		return PASSWORD;
	}

	/**
	 * @return the ocFees
	 */
	public static int getOcFees() {
		return OC_FEES;
	}

	/**
	 * @return the bcFees
	 */
	public static int getBcFees() {
		return BC_FEES;
	}

	/**
	 * @return the scFees
	 */
	public static int getScFees() {
		return SC_FEES;
	}

	/**
	 * @return the stFees
	 */
	public static int getStFees() {
		return ST_FEES;
	}
    /**
     * this method sets the default userName and password for the student 
     */
	public  void setUserNamePass() {
		this.userName = this.studentId;
		this.passWord = PASSWORD;
	}
	
	
	/**
	 * 
	 * this method calculates the due based on cast and fees paid by student
	 * 
	 * @param int cast,double paidFees
	 */
	public void calculateDueAndSet(String cast, double paidFees) {
       
		switch (cast) {
		case "OC":
			setTotalFee(OC_FEES);
			break;
		case "BC":
			setTotalFee(BC_FEES);
			break;
		case "SC":
			setTotalFee(SC_FEES);
			break;
		case "ST":
			setTotalFee(ST_FEES);
			break;
		default:
			break;
		}
		 double totalFee = getPusrsuingYear() * getTotalFee();
		 double totalPaidFee = getFees()+paidFees;
		 
		// this line sets the due to the student object
		setFees(totalPaidFee);
		setDue(totalFee - totalPaidFee);
	}

	/**
	 * toString is override to show the user details
	 */
/**
 * this method returns the pursuying year of the student it calcluts by current year and joining year 
 * @return pursuing year 
 * 
 */
	public int getPusrsuingYear() {
		int pursuingYear ;
		if(this.Qualification.equalsIgnoreCase("inter")) {
			 pursuingYear = SYSTEM_YEAR - JoiningYear;
		}
		else
			pursuingYear = SYSTEM_YEAR - JoiningYear + 1;
		
		if(SYSTEM_MONTH > 3) {
			pursuingYear += 1;
		}
		return pursuingYear;
	}
	public String toString() {
		System.out.println("Student ID:" + this.studentId);
		System.out.println("First Name:" + this.firstName);
		System.out.println("Last Name:" + this.lastname);
		System.out.println("Date Of Birth:" + this.dateOfBirth);
		System.out.println("Mobile Number:" + this.mobileNo);
		System.out.println("Email ID:" + this.email);
		System.out.println("Gender:" + this.gender);
		System.out.println("Address:" + this.address);
		System.out.println("Qualification:" + this.Qualification);
		System.out.println("Caste:" + this.caste);
		System.out.println("joining year:" + this.getJoiningYear());
		System.out.println("Total Fee:" + this.totalFee);
		System.out.println("Fees:" + this.fees);
		System.out.println("Dues:" + this.due);
		return " ";
	}
}
