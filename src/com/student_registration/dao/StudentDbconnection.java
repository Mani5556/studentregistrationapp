package com.student_registration.dao;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.TreeMap;

import com.student_registration.models.StudentModel;
import com.student_registration.utilities.UtilityDao;

/**
 * this class is used for All the CURD operations on student file
 * 
 * @author Group B
 *
 */
public class StudentDbconnection {
	public static final String STUDENT_FILE_PATH = "StudentInfo.txt";
	private UtilityDao utilityDao;
    public StudentDbconnection() {
		utilityDao = new UtilityDao();
	}
	


	/**
	 * this method read the Students details as a TreeMap
	 * 
	 * @return TreeMap of all the students
	 */
	public  TreeMap<String, StudentModel> getStudentsList() {

		ObjectInputStream inStreem = utilityDao.getInputStreem(STUDENT_FILE_PATH);
		// if InputStreem is null then there is no Studentfile so we create the file
		// with empty students list
		if (inStreem == null) {
			ObjectOutputStream out = utilityDao.getOutPutStreem(STUDENT_FILE_PATH);
			try {
				out.writeObject(new TreeMap<String, StudentModel>());
				// after that we have to create inputStrem connection to that file
				inStreem = utilityDao.getInputStreem(STUDENT_FILE_PATH);
			} catch (IOException e) {
			} finally {
				try {
					out.close();
				} catch (IOException e) {
				}
			}
		}

		try {
			@SuppressWarnings("unchecked")
			TreeMap<String, StudentModel> studentsList = (TreeMap<String, StudentModel>) inStreem.readObject();
			return studentsList;
		} catch (ClassNotFoundException e) {
		} catch (IOException e) {
		}

		try {
			inStreem.close();

		} catch (IOException e) {
		}

		return null;

	}

	/**
	 * this method reset the Students list with the latest data into the file
	 * 
	 * @param adminList
	 */
	public  void setStudentsList(TreeMap<String, StudentModel> StudentsList) {
		ObjectOutputStream outStreem = utilityDao.getOutPutStreem(STUDENT_FILE_PATH);
		if (outStreem != null) {
			try {
				outStreem.writeObject(StudentsList);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		try {
			outStreem.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
