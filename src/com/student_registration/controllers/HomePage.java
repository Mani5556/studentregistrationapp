package com.student_registration.controllers;

import java.util.Scanner;

import com.student_registration.utilities.CommonClass;

/**
 * this class is the entry point for the project
 * 
 * @author Group B
 *
 */
public class HomePage {
	CommonClass common = new CommonClass();

	public static void main(String[] args) {
		HomePage home = new HomePage();
		// Showing the home page option
		home.showHomePage();
		
	}

	/**
	 * this method show the home page ,showing the options like admin and student
	 * 
	 */
	public void showHomePage() {
		Scanner scan = new Scanner(System.in);
		AdminPage adminPageObj = new AdminPage();
		StudentPage studentPage = new StudentPage();
	    outerWhileLoop: while (true) {
			System.out.println("                            STUDENT REGISTRATION APPLICATION                ");
			System.out.println("             ================================================================  \n");
			int choice = -1;
			whileLoop: while (true) {
				System.out.println("Select your Option:");
				System.out.println("------------------");
				System.out.println("1.Admin\n2.Student\n3.Exit");
				try {
					choice = Integer.parseInt(scan.next());
					break whileLoop;
				} catch (Exception e) {
					
					System.out.println("Please Enter Only Integer Values...!!!");
				}
			}
			switch (choice) {

			case 1: // this method show the registration form for the admin
				adminPageObj.adminHomePage();
				break outerWhileLoop;
			case 2:
				studentPage.studentHomePage();
				break outerWhileLoop;
			case 3:
				System.out.println("Thank you ...");
				System.exit(0);
			default:
				System.out.println("Please Choose Correct Option");
				break;
			}
		 }
	 scan.close();
	}

}
