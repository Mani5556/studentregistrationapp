package com.student_registration.controllers;

import java.util.ArrayList;
import java.util.Scanner;
import com.student_registration.models.StudentModel;
import com.student_registration.models.UtilityClass;
import com.student_registration.services.StudentService;
import com.student_registration.utilities.CommonClass;
import com.student_registration.utilities.UtilityDao;

/**
 * This class taking the input rom the user for the regestration of new user
 * this class fills all the student deatils from the user and stores into the
 * file
 * 
 * @author IMVIZAG
 *
 */
public class StudentPage {
	CommonClass common;
	StudentModel student;
	StudentInputTaking input;
	public static final String STUDENT_ID_PREFIX = "CN00";
    StudentService studentService;
	public StudentPage() {
		common = new CommonClass();
		student = new StudentModel();
		input = new StudentInputTaking();
		studentService = new StudentService();
	}

	/**
	 * this method shows the student login page
	 */
	public void studentHomePage() {
		int studentLoginAttempts = 0;
		common.showLoginForm( studentLoginAttempts, "Student");
	}

	/**
	 * this method shows the student registration form and stores the new student
	 * details into the studentsInfo file
	 * 
	 */

	boolean showStudentRegisterForm() {
		StudentModel student = new StudentModel();
        System.out.println("                        Student Registration Form   \n                ----------------------------------------------");
		// this code is getting the UtilityClass Obj
		UtilityDao utilityDao = new UtilityDao();
        UtilityClass utility = utilityDao.getUtilityObject();
		int lastStudentId ;
		if (utility != null) {
			lastStudentId = utility.getLastStuddentId();
		} else
			return false;

		System.out.print("Student Id:");
		String newstudentId = (STUDENT_ID_PREFIX + (lastStudentId + 1));
		student.setStudentId(newstudentId);
		System.out.println(newstudentId);

		// Calling askingFirstName() and storing in one variable
		String studentFirstName = input.askingName("FirstName");
		student.setFirstName(studentFirstName);

		// Calling askingLastName() and storing in one variable
		String studentLastName = input.askingName("LastName");
		student.setLastname(studentLastName);

		// Taking the input email of student and verifying pattern using a method
		// dateOfBirthValidation
		String studentDOB = input.askingDateOfBirth();
		student.setDateOfBirth(studentDOB);

		// Taking the input mobile number of student and verifying pattern using a
		// method mobileNumberValidation
		String studentMobileNo = input.askingMobileNumber();
		student.setMobileNo(studentMobileNo);

		// Taking the input email of student and verifying pattern using a method
		// emailValidation
		String studentEmail = input.askingEmailID();
		student.setEmail(studentEmail);

		// Taking the input Gender using Switch case:
		String gender = input.askingGender();
		student.setGender(gender);

		// Taking the input student Address and calling the method address validation
		String studentAddress = input.askingStudentAddress();
		student.setAddress(studentAddress);

		String studentQualification = input.askingStudentQualification();
		student.setQualification(studentQualification);

		// Taking the inputcaste of student by using switch case
		String caste = input.askingCaste();
		student.setCaste(caste);

		// Taking the input Course of student and validating it by calling
		// stringValidation method
		String studentCourse = input.askingStudentCourse();
		student.setCourse(studentCourse);

		double feesPaid = input.askingFeesPaid();
		// this call sets the student due based on his cast
		student.setFees(feesPaid);
		student.calculateDueAndSet(student.getCaste(), feesPaid);

		// this call stores the ``````` user name as register id and default password as
		// 1234
		student.setUserNamePass();
		// sets the joining year of student 
		int joiningYear= input.askingJoiningYear();
		student.setJoiningYear(joiningYear);

		// storing the student object into the StudentInfo file
		boolean registerStatus = studentService.registerStudent(student);
		if (registerStatus) {
			utility.setLastStuddentId(lastStudentId + 1);
			utilityDao.updateUtilityObj(utility);
			return true;

		} else
			System.out.println("Ooops!! registration failed ");
			return false;

	}

	/**
	 * this method shows the student options to after student login Successfully
	 * 
	 * @param StudentModel Object
	 * 
	 *
	 */

	public  void studentOperations(StudentModel student) {
		System.out.println("                                    Welcome " + student.getFirstName());
		System.out.println("                             ------------------------------------------------");
		if(student.getDue() > 0)
		System.out.println("                                                      Hey  "+ student.getFirstName() + " you have " + student.getDue() + "/- due ");
		Scanner scan = new Scanner(System.in);
		int option = -1;
		StudentPage studentPage = new StudentPage();
		outerWhileLoop: while (true) {
			innerWhileLoop: while (true) {
				System.out.println("Select your option:");
				System.out.println( "------------------");
				System.out.println("1.student Details\n2.reset password\n3.View marks\n4.logout");
				try {
					option = Integer.parseInt(scan.next());
					break innerWhileLoop;
				} catch (Exception e) {
					System.out.println("Please Enter Integer only...!!!");
				}
			}
			switch (option) {
			case 1:
				studentPage.showStudentDetails(student);
				break ;
			case 2:
				boolean resetStatus = new CommonClass().resetPassword("Student", student.getStudentId());
				if (resetStatus) {
					System.out.println("password changed successfully");
					System.out.println("----------------------------------------------------------------------------------------");
				} else {
					System.out.println("Oops some thing went wrong ");
				}
				break ;
			case 3:
				common.showMarks(student.getStudentId());
				break;

			case 4:
				HomePage homePage = new HomePage();
				System.out.println("----------------------------------------------------------------------------------------");
				homePage.showHomePage();
				break outerWhileLoop;

			default:
				System.out.println("please enter the valid input");
				break;
			}
		}
		scan.close();
	}

	/**
	 * this method shows the student detsils to the user
	 * 
	 * @param StudentModel object
	 * 
	 */
	private void showStudentDetails(StudentModel student) {
		//System.out.println(student);
		ArrayList<StudentModel> students = new ArrayList<StudentModel>();
		students.add(student);
		AdminPage.showStudentDetails(students);
	}

}
