package com.student_registration.services;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;

import com.student_registration.dao.StudentDbconnection;
import com.student_registration.enums.EnumClasses;
import com.student_registration.models.StudentModel;

/**
 * this class is having the all the different searching methods for the student
 * details
 */
public class SearchStudent {
	Scanner scanner = new Scanner(System.in);
	static int systemYear = Calendar.getInstance().get(Calendar.YEAR);
	StudentDbconnection studentDbconnection;
	public SearchStudent() {
		studentDbconnection = new StudentDbconnection();
	}

	/**
	 * this method searches the student details by Id
	 * 
	 * @param Sting Student Id
	 * @return StudentModel object
	 */
	public StudentModel searchById(String studentId) {
		TreeMap<String, StudentModel> studentslist = studentDbconnection.getStudentsList();
		return studentslist.get(studentId);
	}

	public ArrayList<StudentModel> search(EnumClasses.SearchKeys context, String input,ArrayList<StudentModel> searchedList) {
		ArrayList<StudentModel> studentsList;
		ArrayList<StudentModel> foundStudents = new ArrayList<StudentModel>();
		if(searchedList != null) {
			studentsList = searchedList;
		}
		else {
			studentsList = new ArrayList<StudentModel>();
			TreeMap<String, StudentModel> list = studentDbconnection.getStudentsList();
			if (list != null) {
				Set<String> keys = list.keySet();
				for (String key : keys) {
					studentsList.add(list.get(key));
				}
			}
		}
		
		for (StudentModel student : studentsList) {
			switch (context) {
			case ALL:
			    foundStudents.add(student);
			    break;
			case COURSE:
				if (student.getCourse().toLowerCase().contains(input)) {
					foundStudents.add(student);
				}
				break;

			case NAME:
				if (student.getFirstName().toLowerCase().contains(input)
						|| student.getLastname().toLowerCase().contains(input)) {
					foundStudents.add(student);
				}
				break;
			case DUE:
				if (student.getDue() > 0) {
					foundStudents.add(student);
				}
				break;
			case CAST:
				if (student.getCaste().toLowerCase().contains(input)) {
					foundStudents.add(student);
				}
				break;
			case YEAR:
				int studentCurrentYear = student.getPusrsuingYear();
				if (studentCurrentYear == Integer.parseInt(input)) {
					foundStudents.add(student);
				} else if (studentCurrentYear > 5 && Integer.parseInt(input) == 5) {
					foundStudents.add(student);
				}
				break;
			case BACKLOGS:
				if (student.getBacklogs() > 0) {
					foundStudents.add(student);
				}
				break;
			case GENDER:
				if(student.getGender().equalsIgnoreCase(input)) {
					foundStudents.add(student);
				}

			default:
				break;
			}

		}
		return foundStudents;
	}

	// void searchBy

}
