package com.student_registration.services;
import java.util.Set;
import java.util.TreeMap;

import com.student_registration.dao.AdminDbConnection;
import com.student_registration.models.AdminModel;

/**
 * this class handles the all the logics on admin module 
 * @author GROUP-B
 *
 */
public class AdminService {
    TreeMap<Integer,AdminModel> adminList;
	AdminDbConnection adminDbConnection = new AdminDbConnection();
	public static final String UNIQKEY = "GROUP-B";
    
	/**
	 * this method insert the data into the AdminInfo File ...
	 * 
	 * @param admin is AdminModel type
	 * @return true if admin registers successfully else false
	 */
	
	public  void signUpAdmin(AdminModel admin) throws Exception {
		if (admin.getUniqueId().equals(UNIQKEY)) {
			adminList = adminDbConnection.getAdminList();
			if (adminList != null) {
				Set<Integer> keys = adminList.keySet();
				for(Integer key : keys) {
					if( adminList.get(key).getAdminUserName().equals(admin.getAdminUserName())) {
						throw new Exception("Already same user exists with this username..");
					}
				}
				adminList.put(keys.size() + 1, admin);
				adminDbConnection.setAdminList(adminList);
			}
			else {
			    throw new Exception("Some thing went wrong");
			}
			
		} else
			throw new Exception("Entered uniqueId is not matched");
	}
	
	/**
	 * this method checks the admin details,if found return the boolen value
	 * 
	 * @param adminInfo
	 * @return true if the admin is Valid admin else return false
	 */

	public AdminModel adminLogin(String username,String password) {
		adminList = adminDbConnection.getAdminList();
		Set<Integer> keys = adminList.keySet();
		for (Integer i : keys) {
			AdminModel admin = adminList.get(i);
			if (admin.getAdminUserName().equals(username)
					&& admin.getAdminPassword().equals(password)) {
				return admin;
			}
		}
		return null;
	}
	

	/**
	 * this method searching the admin by the username and security code returns the
	 * key of the admin
	 * 
	 * @param adminForgotDetails
	 * @return admin index in treeMap of admins
	 */

	public String adminForgotPassWord(String userName,String securityQue) {
		adminList = adminDbConnection.getAdminList();
		Set<Integer> keys = adminList.keySet();
		for (Integer i : keys) {
			AdminModel admin = adminList.get(i);
			if (admin.getAdminUserName().equals(userName)
					&& admin.getAdminSecurityQue().equals(securityQue)) {
				return i.toString();
			}
		}
		return null;
	}
	
	/**
	 * this method resets the admin password
	 * 
	 * @param object index or key of map ,new password details
	 * 
	 */
	public  boolean resetAdminPassword(String newPassWord, int searchKey) {
		TreeMap<Integer, AdminModel> adminsList = adminDbConnection.getAdminList();
		if (adminsList == null)
			return false;
		AdminModel admin = adminsList.get(searchKey);
		if(admin != null) {
			admin.setAdminPassword(newPassWord);
			adminsList.put(searchKey, admin);
			adminDbConnection.setAdminList(adminsList);
			return true;
		}
		else 
			return false;
	}


}
