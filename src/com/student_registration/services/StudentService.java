package com.student_registration.services;

import java.util.Set;
import java.util.TreeMap;

import com.student_registration.dao.StudentDbconnection;
import com.student_registration.models.StudentModel;

/**
 * this class is handles all the logics of the student 
 * @author IMVIZAG
 *
 */
public class StudentService {
	private StudentDbconnection studentDbconnection;
	/**
	 * this constructor creates the studenDbConnection object
	 */
	public StudentService() {
	     studentDbconnection = new StudentDbconnection();
	}
	
	
	/**
	 * this method regers the new student into the StudentInfo file
	 * 
	 * @param StudentModel Object
	 * @return true if the student is regestered else return false
	 */

	public boolean registerStudent(StudentModel student) {

		TreeMap<String, StudentModel> studentsList =  studentDbconnection.getStudentsList();
		if (studentsList != null) {
			studentsList.put(student.getStudentId(), student);
			studentDbconnection.setStudentsList(studentsList);
			return true;
		}
		return false;
	}
	
	/**
	 * this method logins the user if the given credentials matches to the database
	 * else return false
	 * 
	 * @param student name and password
	 * @return Studnet Object if loginCredentials matches else return false
	 */
	public StudentModel studentLogin(String userName, String password) {

		TreeMap<String, StudentModel> studentsList = studentDbconnection.getStudentsList();
		if (studentsList != null) {
			Set<String> keys = studentsList.keySet();
			for (String key : keys) {
				if (studentsList.get(key).getUserName().equals(userName)
						&& (studentsList.get(key).getPassWord().equals(password))) {
					return studentsList.get(key);
				}
			}
		}
		return null;
	}
	
	/**
	 * This updates the student existing details in the file
	 * 
	 * @param StudentModel Object return true if updation is succesful else it
	 *                     return false
	 */
	public  boolean updateStudent(StudentModel student) {
		return registerStudent(student);
	}

	/**
	 * this method finds the student details if the student found with userId and
	 * seqQue
	 * 
	 * @param String studentId as the key fro the studentsList map and seqQue
	 * @return boolen
	 */
	public  String studentForgotPassWord(String studentID, String sequrityQuestion) {
		TreeMap<String, StudentModel> studentsList = studentDbconnection.getStudentsList();
		StudentModel student = studentsList.get(studentID);
		if (student != null && student.getStudentId().equals(studentID) && student.getSecurityQue().equals(sequrityQuestion)) {
			return studentID;
		}
		return null;
	}

	/**
	 * this method reset the student password 
	 * @param studentId,new Password,favPlace
	 * @return boolean
	 */
	public boolean resetPassword(String studentId, String newPassword, String favPlace) {
		TreeMap<String, StudentModel> studentsList = studentDbconnection.getStudentsList();
		if (studentsList != null) {
			StudentModel student = studentsList.get(studentId);
			if(student != null) {
				student.setPassWord(newPassword);
				student.setFavoritePlace(favPlace);
				studentDbconnection.setStudentsList(studentsList);
				return true;
			}
			else 
				return false;
		} else
			return false;
	}

	/**
	 * this method delets the student if the student found ,and updates the last
	 * studentId in the UtilityFile
	 * @param studentId
	 */

	// TODO COMPLETE THE DELETE METHOD
	public  boolean deleteStudent(String studentId) {
		TreeMap<String, StudentModel> studentsList = studentDbconnection.getStudentsList();
		if (studentsList != null) {
			StudentModel student = studentsList.get(studentId);
			if (student != null) {
				studentsList.remove(studentId);
				studentDbconnection.setStudentsList(studentsList);

			} else {
				System.out.println("No student student found with this id");
				return false;
			}
		}
		return true;
	}

}
