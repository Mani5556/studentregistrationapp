package com.student_registration.validations;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

/**
 * This class is maintained for validating the admin details like username and
 * password
 * 
 * @author Batch B
 *
 */
public class AdminValidations {

	private static final String PASSWORD_PATTERN = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,15})";
	private static final String ADMIN_USERNAME = "^[a-zA-Z0-9]*$";

	/**
	 * This Method is used to validate the username of admin
	 * 
	 * @return true if its valid or else false
	 * @param adminUserName
	 */
	public boolean adminUserNameValidation(String adminUserName) {
		Pattern userNamePattern = Pattern.compile(ADMIN_USERNAME);
		Matcher matchUserName = userNamePattern.matcher(adminUserName);
		if (matchUserName.matches()) {
			return true;
		}
		return false;
	}

	/**
	 * This Method is used to validate the password of admin
	 * 
	 * @param adminPassword
	 * @return true if its valid or else false
	 */
	public static boolean adminPasswordValidate(String adminPassword) {
		Pattern passwordPattern = Pattern.compile(PASSWORD_PATTERN);
		Matcher matchPassword = passwordPattern.matcher(adminPassword);
		if (matchPassword.matches()) {
			return true;
		}
		return false;
	}
}
